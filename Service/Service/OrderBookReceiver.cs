﻿using ExchangeSharp;
using Service.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Service
{
    public class OrderBookReceiver
    {
        private readonly IDictionary<string, IExchangeAPI> apis;
        public event Action<IEnumerable<ExchangeOrderBook>> OrderBooksReceived;

        public OrderBookReceiver()
        {
            var apis = new Dictionary<string, IExchangeAPI>();
            foreach (string exchnageName in ProgramSettings.AvailableExchanges)
            {
                if (!ExchangeName.HasName(exchnageName))
                {
                    throw new APIException($"Api for exchnage {exchnageName} not exist");
                }
                apis.Add(exchnageName, ExchangeAPI.GetExchangeAPI(exchnageName));
            }
            this.apis = apis;
        }

        public void Start()
        {
            while (true)
            {
                foreach (string symbol in ProgramSettings.AvailableSymbols)
                {
                    var tasks = new List<Task<ExchangeOrderBook>>(ProgramSettings.AvailableExchanges.Length);
                    foreach (string exchangeName in ProgramSettings.AvailableExchanges)
                    {
                        Task<ExchangeOrderBook> task = Task.Run(() =>
                       {
                           return GetOrderBook(exchangeName, symbol);
                       });
                        tasks.Add(task);
                        //Task task = new Task(new Action<object>(GetOrderBook), new Param(exchangeName, symbol));
                        //ExchangeOrderBook orderBook = GetOrderBook(symbol, exchangeName);
                        //OrderBookReceived?.Invoke(orderBook);
                    }
                    Task.WaitAll(tasks.ToArray());
                    List<ExchangeOrderBook> orderBooks = new List<ExchangeOrderBook>(ProgramSettings.AvailableExchanges.Length);
                    foreach (Task<ExchangeOrderBook> task in tasks)
                    {
                        orderBooks.Add(task.Result);
                    }
                    this.OrderBooksReceived?.Invoke(orderBooks);
                }
                Thread.Sleep(1000);
            }
        }

        private ExchangeOrderBook GetOrderBook(string exchangeName, string symbol)
        {
            string localSymbol = apis[exchangeName].GlobalMarketSymbolToExchangeMarketSymbol(symbol);
            try
            {
                ExchangeOrderBook orderBook = apis[exchangeName].GetOrderBookAsync(localSymbol).Result;
                orderBook.MarketSymbol = symbol;
                orderBook.ExchangeName = exchangeName;
                return orderBook;
            }
            catch
            {
                return new ExchangeOrderBook();
            }
        }
    }
}
