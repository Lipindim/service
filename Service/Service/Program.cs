﻿using Kernel;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Service
{
    class Program:ConsoleProgram
    {
        private static readonly OrderBookAggregator OrderBookAggregator = new OrderBookAggregator();

        /// <summary>
        /// Gets program name.
        /// </summary>
        public override string Name => "Service";

        /// <summary>
        /// Logger instance.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">Application arguments.</param>
        /// <returns>Program result code.</returns>
        private static int Main(string[] args)
        {
            return MainCommon<Program>(args);
        }

        /// <summary>
        /// Initializes application inner state.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Executes program with arguments.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        protected override void Execute(string[] args)
        {
            Logger.Info("Running...");

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            OrderBookReceiver orderBookReceiver = new OrderBookReceiver();
            orderBookReceiver.OrderBooksReceived += OrderBookReceiver_OrderBookReceived;
            orderBookReceiver.Start();
        }

        /// <summary>
        /// Occurs when program is about to be terminated.
        /// </summary>
        protected override void OnTerminating()
        {
            base.OnTerminating();

            Logger?.Info("Terminated.");
        }

        /// <summary>
        /// Occurs when program is finished.
        /// </summary>
        protected override void OnFinish()
        {
            base.OnFinish();

            Logger?.Info("Finished.");
        }

        /// <summary>
        /// Occures when program is finished with an error.
        /// </summary>
        /// <param name="ex">Occurred exception.</param>
        protected override void OnError(Exception ex)
        {
            base.OnError(ex);

            Logger?.Error(ex, "Main application error.");
        }


        private static void OrderBookReceiver_OrderBookReceived(IEnumerable<ExchangeSharp.ExchangeOrderBook> orderBooks)
        {
            foreach (var orderBook in orderBooks)
            {
                OrderBookAggregator.UpdateOrderBook(orderBook);
            }
            
            var aggregatedOrderBook = OrderBookAggregator.GetAggregatedOrderBook(orderBooks.First().MarketSymbol);
            Console.WriteLine(new string('*', 20));
            Console.WriteLine(DateTime.Now.ToString("HH:mm:ss,fff"));
            Console.WriteLine(orderBooks.First().MarketSymbol);
            int i = 0;
            foreach (var item in aggregatedOrderBook.Asks)
            {
                Console.WriteLine($"{item[2]} {item[0]} {item[1]}");
                //i++;
                //if (i >= 50)
                //{
                //    break;
                //}
            }
            Console.WriteLine($"Summ: {aggregatedOrderBook.Asks.Sum(x => (decimal)x[1])}");
        }
    }
}
