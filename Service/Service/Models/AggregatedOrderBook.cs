﻿using System.Collections.Generic;

namespace Service.Models
{
    public class AggregatedOrderBook
    {
        public string Symbol { get; set; }

        //0-price, 1-amount, 2-exchange
        public List<object[]> Asks { get; set; }

        public List<object[]> Bids { get; set; }

        public AggregatedOrderBook(string symbol)
        {
            Symbol = symbol;
        }
    }
}
