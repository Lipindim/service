﻿using ExchangeSharp;
using Service.Models;
using System.Collections.Generic;

namespace Service
{
    public class OrderBookAggregator
    {
        private readonly Dictionary<string, AggregatedOrderBook> orderBooks = new Dictionary<string, AggregatedOrderBook>();

        public OrderBookAggregator()
        {
            foreach (string symbol in ProgramSettings.AvailableSymbols)
            {
                orderBooks.Add(symbol, new AggregatedOrderBook(symbol));
            }
        }

        public AggregatedOrderBook GetAggregatedOrderBook(string symbol)
        {
            if (orderBooks.TryGetValue(symbol, out AggregatedOrderBook aggregatedOrderBook))
            {
                return aggregatedOrderBook;
            }

            return null;
        }

        public void UpdateOrderBook(ExchangeOrderBook orderBook)
        {
            if (string.IsNullOrWhiteSpace(orderBook.MarketSymbol))
            {
                return;
            }
            if (orderBooks.TryGetValue(orderBook.MarketSymbol, out AggregatedOrderBook aggregatedOrderBook))
            {
                if (aggregatedOrderBook.Asks == null || aggregatedOrderBook.Asks.Count == 0)
                {
                    aggregatedOrderBook.Asks = this.GetNewOrderPriceCollection(orderBook.ExchangeName, orderBook.Asks);
                }
                else
                {
                    this.AggtegateOrderPriceCollection(orderBook, aggregatedOrderBook.Asks);
                }

                if (aggregatedOrderBook.Bids == null || aggregatedOrderBook.Bids.Count == 0)
                {
                    aggregatedOrderBook.Bids = this.GetNewOrderPriceCollection(orderBook.ExchangeName, orderBook.Bids);
                }
                else
                {
                    this.AggtegateOrderPriceCollection(orderBook, aggregatedOrderBook.Bids, false);
                }
            }
            else
            {
                throw new APIException($"Symbol {orderBook.MarketSymbol} not supported");
            }
        }

        private void AggtegateOrderPriceCollection(ExchangeOrderBook orderBook, List<object[]> orderPriceList, bool isAsk = true)
        {
            orderPriceList.RemoveAll(x => (string)x[2] == orderBook.ExchangeName);
            int aggregatedIndex = 0;
            var orderPriceCollection = isAsk ? orderBook.Asks.Values : orderBook.Bids.Values;
            foreach (ExchangeOrderPrice orderPrice in orderPriceCollection)
            {
                if (aggregatedIndex < orderPriceList.Count)
                {
                    while ((orderPrice.Price > (decimal)orderPriceList[aggregatedIndex][0] && isAsk)
                        || (orderPrice.Price < (decimal)orderPriceList[aggregatedIndex][0] && !isAsk))
                    {
                        aggregatedIndex++;
                        if (aggregatedIndex >= orderPriceList.Count)
                        {
                            break;
                        }
                        if (aggregatedIndex >= 100)
                        {
                            break;
                        }
                    }
                }
                object[] orderPriceArray = new object[3];
                orderPriceArray[0] = orderPrice.Price;
                orderPriceArray[1] = orderPrice.Amount;
                orderPriceArray[2] = orderBook.ExchangeName;
                orderPriceList.Insert(aggregatedIndex, orderPriceArray);

                aggregatedIndex++;
                if (aggregatedIndex >= 100)
                {
                    break;
                }
            }
            orderPriceList.RemoveRange(100, orderPriceList.Count - 100);
        }

        private List<object[]> GetNewOrderPriceCollection(string exchangeName, SortedDictionary<decimal, ExchangeOrderPrice> exchangeOrderPrices)
        {
            List<object[]> asks = new List<object[]>();
            foreach (ExchangeOrderPrice exchangeOrderPrice in exchangeOrderPrices.Values)
            {
                object[] orderPriceArray = new object[3];
                orderPriceArray[0] = exchangeOrderPrice.Price;
                orderPriceArray[1] = exchangeOrderPrice.Amount;
                orderPriceArray[2] = exchangeName;
                asks.Add(orderPriceArray);
            }
            return asks;
        }
    }
}
