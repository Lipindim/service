﻿namespace Service
{
    public static class ProgramSettings
    {
        public static string[] AvailableSymbols { get; set; } = new string[] { "BTC-USDT", "LTC-BTC" };
        public static string[] AvailableExchanges { get; set; } = new string[] { "Binance", "Bittrex", "Huobi", "Hitbtc", "Okex", "Coinbase", "Poloniex" };
    }
}
