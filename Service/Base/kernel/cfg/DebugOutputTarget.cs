﻿namespace Kernel
{
    using System.Diagnostics;
    using System.Text.RegularExpressions;
    using NLog;
    using NLog.Config;
    using NLog.Targets;

    public class DebugOutputTarget : TargetWithLayout
    {
        public static void AddTarget(bool reconfig = true)
        {
            var logTarget = new DebugOutputTarget
            {
                Layout =
                    @"${callsite:className=false:fileName=true:includeSourcePath=true:methodName=false} ${date:format=HH\:mm\:ss} ${message} ${exception}"
            };


            // specify what gets logged to the above target
            var loggingRule = new LoggingRule("*", LogLevel.Debug, logTarget);

            // add target and rule to configuration
            LogManager.Configuration.AddTarget("DebugOutputTarget", logTarget);
            LogManager.Configuration.LoggingRules.Add(loggingRule);
            if (reconfig)
                LogManager.ReconfigExistingLoggers();
        }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = this.Layout.Render(logEvent);
            Regex regex = new Regex(@"^\((.+):(\d+)\)(.+)");
            logMessage = regex.Replace(logMessage, "$1($2): $3");

            Debug.WriteLine(logMessage);
        }
    }
}