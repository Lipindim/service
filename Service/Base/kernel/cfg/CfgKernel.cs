﻿namespace Kernel
{
    using McMaster.Extensions.CommandLineUtils;
    using Microsoft.Extensions.Configuration;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Linq;

    public static class CfgKernel
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static IConfigurationRoot configuration;
        private static object lockObj = new object();
        private const string baseCfgDir = "configs";
        static string appName = string.Empty;
        private static string appNameNVersion = string.Empty;
        private static string kernelFolder = string.Empty;
        private static string appFolder = string.Empty;
        private static string baseKernelConfigPath = string.Empty;
        private static string baseKernelConfigPathNSep = string.Empty;
        private static string baseAppConfigPath = string.Empty;
        private static string baseAppConfigPathNSep = string.Empty;
        private static string appLogId { get { return OptionProdMode.HasValue() ? appName : appName + "Dev"; } }

        private static string defaultConfig { get; set; }
        private const string defaultConfigFName = "dev.kernel.yaml";
        private const string defaultUserConfigFName = "default.yaml";
        private const string defaultProdConfigFName = "prod.kernel.yaml";
        private const string defaultNLogDefConfigFName = "dev.kernel.nlog.config";
        private const string defaultNLogProdConfigFName = "prod.kernel.nlog.config";
        public static string defaultNLogConfigName { get { return (OptionProdMode.HasValue() ? defaultNLogProdConfigFName : defaultNLogDefConfigFName); } }

        public static CommandOption OptionProdMode { get; set; }
        public static CommandOption OptionConfig { get; set; }
        public static CommandOption OptionLogToGoogleAuth { get; set; }
        public static CommandOption OptionLogToGoogle { get; set; }
        public static CommandOption OptionWireType { get; set; }
        public static CommandOption OptionNLogConfig { get; set; }
        public static CommandOption OptionFileLoggerMinlevel { get; set; }
        public static CommandOption OptionConsoleLoggerMinlevel { get; set; }
        public static CommandOption OptionGoogleLoggerMinlevel { get; set; }

        private static LogLevel googleLoggerMinlevel = LogLevel.Trace;
        public static LogLevel GoogleLoggerMinlevel { get { return googleLoggerMinlevel; } set { googleLoggerMinlevel = value; setLoggingLevel("stackdriver", value); } }

        private static LogLevel fileLoggerMinlevel = LogLevel.Trace;
        public static LogLevel FileLoggerMinlevel { get { return fileLoggerMinlevel; } set { fileLoggerMinlevel = value; setLoggingLevel("file", value); } }

        private static LogLevel consoleLoggerMinlevel = LogLevel.Trace;
        public static LogLevel ConsoleLoggerMinlevel { get { return consoleLoggerMinlevel; } set { consoleLoggerMinlevel = value; setLoggingLevel("console", value); } }


        private static IList<string> userConfigs { get; set; }

        public static string EnvironmentName
        {
            get
            {
                return OptionProdMode.HasValue() ? "prod" : "dev";
            }
        }

        public static string appVersion { get; set; } = string.Empty;
        public static bool NoAuxOutput { get; set; }

        public static IConfigurationRoot Configuration
        {
            get
            {
                if (configuration == null)
                    throw new Exception("Please call Kernel.Cfg.ConfigureApplication() in entry point, before any other code in your application");

                return configuration;
            }

            private set { configuration = value; }
        }

        public static void ConfigureApplication()
        {
            lock (lockObj)
            {
                if (configuration != null)
                {
                    return;
                }
                ConsoleSink consoleSink = NoAuxOutput ? new ConsoleSink() : null;

                try
                {
                    AssemblyName entryAssemblyName = Program.EntryAssembly.GetName();

                    appName = entryAssemblyName.Name;

                    if (string.IsNullOrEmpty(appName))
                    {
                        appName = "Noname";
                    }
                    // to uppercase
                    char[] appNameArr = appName.ToCharArray();
                    appNameArr[0] = char.ToUpperInvariant(appNameArr[0]);
                    appName = new string(appNameArr);

                    appVersion = entryAssemblyName.Version.ToString();

                    if (string.IsNullOrEmpty(appVersion))
                    {
                        appVersion = "0.0.0";
                    }
                    appNameNVersion = appName + "-" + appVersion;

                    string kernelPath = Assembly.GetExecutingAssembly().Location;
                    Console.WriteLine($"Kernel location: {kernelPath}");
                    kernelFolder = Path.GetDirectoryName(kernelPath);// base dir of dll

                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    Console.WriteLine($"Executable location: {appPath}");
                    
                    appFolder = appPath.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    baseKernelConfigPath = appFolder + Path.DirectorySeparatorChar + baseCfgDir;
                    baseKernelConfigPathNSep = appFolder + Path.DirectorySeparatorChar;

                    baseAppConfigPath = appFolder + Path.DirectorySeparatorChar + baseCfgDir;
                    baseAppConfigPathNSep = baseAppConfigPath + Path.DirectorySeparatorChar;


                    ExtractConfigs();


                    loadConfiguration();

                    if (!NoAuxOutput)
                    {
                        var dumper = new ConfigDumper(line => logger.Info(line));
                        dumper.Dump(Configuration);
                    }
                }

                finally
                {
                    consoleSink?.Dispose();
                }
            }
        }

        public static void ExtractConfigs()
        {
            Console.WriteLine("Extracting configs");
            string configsZip = Path.Combine(appFolder, "configs.zip");

            string fileShouldExist = Path.Combine(baseAppConfigPath, "prod.kernel.yaml");

            if (!File.Exists(fileShouldExist))
            {
                Console.WriteLine($"Extract configs.zip");
                var assembly = Assembly.GetExecutingAssembly();
                using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream("Kernel.configs.zip"))
                {
                    using (var file = new FileStream(configsZip, FileMode.Create, FileAccess.Write))
                    {
                        resource.CopyTo(file);
                        file.Flush();
                        file.Close();
                        System.IO.Compression.ZipFile.ExtractToDirectory(configsZip, appFolder + "/configs");
                        //File.Delete(configsZip);
                    }
                }
            }
            else
            {
                Console.WriteLine($"{fileShouldExist} already exists, will not overwrite");
            }

            // list "configs" directory
            Console.WriteLine("ls configs:");
            FS.Ls(baseAppConfigPath);
        }

        private static IConfigurationRoot loadConfiguration()
        {
            if (configuration != null)
            {
                return configuration;
            }
            string[] args = Environment.GetCommandLineArgs();

            IList<string> argsList = new List<string>(args);
            argsList.RemoveAt(0);
            args = argsList.ToArray();

            var app = new CommandLineApplication(throwOnUnexpectedArg: false)
            {
                Name = appNameNVersion
            };

            AddCLIOptions(app);

            app.Parse(args);


            if (OptionLogToGoogleAuth.HasValue() && !OptionLogToGoogle.HasValue())
            {
                OptionLogToGoogle.TryParse(string.Empty);
            }
            defaultConfig = defaultConfigFName;
            if (OptionProdMode.HasValue())
            {
                defaultConfig = defaultProdConfigFName;
                Console.WriteLine($"Production mode. defaultConfig: {defaultConfig}");
            }
            else
            {
                Console.WriteLine($"Development mode. defaultConfig: {defaultConfig}");
            }

            if (OptionConfig.Values.Count > 0)
            {
                Console.WriteLine($"Applying user config: {string.Join(",", OptionConfig.Values)}");
            }

            userConfigs = OptionConfig.Values;

            app = null;

            Console.WriteLine($"Configs directory: {baseAppConfigPath}");
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(baseAppConfigPath);
            Console.WriteLine($"Loading config: {defaultConfig}");
            builder.AddYamlFile(defaultConfig, optional: false, reloadOnChange: false);

            if (Directory.Exists(baseAppConfigPath))
            {
                Console.WriteLine($"User's configs directory: {baseAppConfigPath}");
                builder.SetBasePath(baseAppConfigPathNSep);
            }
            else
            {
                Console.WriteLine($"No user's configs directory found");
            }
            
            bool defaultUserConfigExists = false;
            if (File.Exists(baseAppConfigPathNSep + defaultUserConfigFName))
            {
                defaultUserConfigExists = true;
                Console.WriteLine($"Loading default user's config: {defaultUserConfigFName}");
                builder.AddYamlFile(defaultUserConfigFName, optional: false, reloadOnChange: true);
            }
            foreach (string userConfig in userConfigs)
            {
                if (!string.IsNullOrWhiteSpace(userConfig))
                {
                    Console.WriteLine($"Loading override user's config: {userConfig}");
                    builder.AddYamlFile(userConfig, optional: false, reloadOnChange: true);
                }
            }

            string userConfigsStr = string.Join(", ", userConfigs);

            Configuration = builder.Build();

            if (OptionWireType.HasValue())
            {
                string wire = OptionWireType.Value();
                Configuration.GetSection("SCWire")["WireType"] = wire;
                Console.WriteLine($"Set SCWire/SatoriWireWireType to {wire} from command line");
            }

            string sourceVariableConfigFilePath = Path.Combine(baseAppConfigPath, "includes", "kernel.nlog.variables.config");
            string destVariableConfigFilePath = Path.Combine(baseAppConfigPath, "includes", "kernel.nlog.initialized.variables.config");

            Console.WriteLine($"Configuring NLog, initialize variables: {destVariableConfigFilePath}");
            setNLogVariables(sourceVariableConfigFilePath, destVariableConfigFilePath);

            string sourceConfigFilePath = Path.Combine(baseAppConfigPath, defaultNLogConfigName);
            if (OptionNLogConfig.HasValue() && null != OptionNLogConfig.Value())
            {
                sourceConfigFilePath = Path.Combine(baseAppConfigPath, OptionNLogConfig.Value());
            }

            string destConfigFilePath = Path.Combine(baseAppConfigPath, defaultNLogConfigName + ".initialized.config");

            Console.WriteLine($"Configuring NLog, setup includes: {destConfigFilePath}");

            setupGoogleTarget(sourceConfigFilePath, destConfigFilePath);
            LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(destConfigFilePath, false);
            
            var addDebugOutput = Configuration["DebugOutputWithSources"] == "true";
            if (addDebugOutput && Debugger.IsLogging())
                DebugOutputTarget.AddTarget(false);
            
            updateLoggingLevel();

            LogManager.ReconfigExistingLoggers();

            if (!NoAuxOutput)
            {
                logger.Info($"NLog configured. Loaded {defaultConfig} "
                            + (defaultUserConfigExists ? "" : " with default user's config " + defaultUserConfigFName)
                            + (string.IsNullOrWhiteSpace(userConfigsStr) ? "" : " with overrides " + userConfigsStr)
                            + " and NLog " + destConfigFilePath);
            }

            return Configuration;
        }

        private static void setupGoogleTarget(string sourceConfigFilePath, string destConfigFilePath)
        {
            if (OptionLogToGoogle.HasValue())
            {
                Console.WriteLine($"Configuring NLog, turning on Google logging");
                if (OptionLogToGoogleAuth.HasValue())
                {
                    Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", baseAppConfigPathNSep + "keys/kernel.google.logger.key.json", EnvironmentVariableTarget.Process);
                }

                File.Copy(sourceConfigFilePath, destConfigFilePath, true);
            }
            else
            {
                Console.WriteLine($"Configuring NLog, removing Google target");

                XDocument doc = XDocument.Load(sourceConfigFilePath);
                XNamespace ns = "http://www.nlog-project.org/schemas/NLog.xsd";

                doc.Descendants(ns + "include")
                   .Where(x => x.Attribute("file").ToString().Contains("kernel.nlog.google.config"))
                   .Remove();

                doc.DescendantNodes().OfType<XComment>().Remove();
                doc.Save(destConfigFilePath);
            }
        }

        private static void setNLogVariables(string sourceConfigFilePath, string destConfigFilePath)
        {
            XDocument doc = XDocument.Load(sourceConfigFilePath);
            XNamespace ns = "http://www.nlog-project.org/schemas/NLog.xsd";

            IDictionary<string, bool> variablePresent = new Dictionary<string, bool> {
                { "appId", false},
                { "appName", false},
                { "appVersion", false},
                { "appNameNVersion", false},
                { "appHostName", false},
                { "appEnvironmentName", false},
                { "appLogId", false}
            };

            foreach (XElement variable in doc.Descendants(ns + "variable"))
            {
                string name = (string)variable.Attribute("name");
                string value;

                variablePresent[name] = true;

                switch (name)
                {
                    case "appId":
                        value = Program.AppId;
                        break;
                    case "appName":
                        value = appName;
                        break;
                    case "appVersion":
                        value = appVersion;
                        break;
                    case "appNameNVersion":
                        value = appNameNVersion;
                        break;
                    case "appHostName":
                        value = Environment.MachineName;
                        break;
                    case "appEnvironmentName":
                        value = EnvironmentName;
                        break;
                    case "appLogId":
                        value = appLogId;
                        break;
                    default:
                        continue;

                }

                variable.SetAttributeValue("value", value);
            }
            
            foreach (string name in variablePresent.Keys)
            {
                if (!variablePresent[name])
                {
                    XElement element = new XElement("variable");
                    XAttribute attribute = new XAttribute("name", name);
                    element.Add(attribute);
                    attribute = new XAttribute("value", "Default");
                    element.Add(attribute);
                }
            }

            doc.DescendantNodes().OfType<XComment>().Remove();
            doc.Save(destConfigFilePath);
        }

        public static void AddCLIOptions(CommandLineApplication app)
        {
            OptionConfig = app.Option(
                "-c|--conf",
                "Apply config",
                CommandOptionType.MultipleValue
            );

            OptionProdMode = app.Option(
                "-p|--prod",
                "Production mode, use base config kernel.prod.default.yaml instead of kernel.default.yaml",
                CommandOptionType.NoValue
            );

            OptionLogToGoogle = app.Option(
                "-g|--log2google",
                "Enable Google cloud logging",
                CommandOptionType.NoValue
            );

            OptionLogToGoogleAuth = app.Option(
                "--log2googleAuth",
                "Enable authenticated Google cloud logging",
                CommandOptionType.NoValue
            );

            // used directly in durov, here it is just to add to --help output
            app.Option(
                "--verbose",
                "Verbose output in durov, i.e. do not set NoAuxOutput. NB should be first option in options list!",
                CommandOptionType.NoValue
            );

            // set logging levels

            OptionGoogleLoggerMinlevel = app.Option(
                "--GoogleLoggerMinlevel",
                @"Set Google cloud logging minLevel <Trace|Debug|Info|Warn|Error|Fatal|Off>
Level\tExample
Fatal\tHighest level: important stuff down
Error\tFor example application crashes / exceptions.
Warn\tIncorrect behavior but the application can continue
Info\tNormal behavior like mail sent, user updated profile etc.
Debug\tExecuted queries, user authenticated, session expired
Trace\tBegin method X, end method X etc
",
                CommandOptionType.SingleValue
            );

            OptionFileLoggerMinlevel = app.Option(
                "--FileLoggerMinlevel",
                @"Set file logging minLevel <Trace|Debug|Info|Warn|Error|Fatal|Off>
Level\tExample
Fatal\tHighest level: important stuff down
Error\tFor example application crashes / exceptions.
Warn\tIncorrect behavior but the application can continue
Info\tNormal behavior like mail sent, user updated profile etc.
Debug\tExecuted queries, user authenticated, session expired
Trace\tBegin method X, end method X etc
",

                CommandOptionType.SingleValue
            );

            OptionConsoleLoggerMinlevel = app.Option(
                "--ConsoleLoggerMinlevel",
                @"Set console logging minLevel <Trace|Debug|Info|Warn|Error|Fatal|Off> 
Level\tExample
Fatal\tHighest level: important stuff down
Error\tFor example application crashes / exceptions.
Warn\tIncorrect behavior but the application can continue
Info\tNormal behavior like mail sent, user updated profile etc.
Debug\tExecuted queries, user authenticated, session expired
Trace\tBegin method X, end method X etc
",
                CommandOptionType.SingleValue
            );



            OptionWireType = app.Option(
                "--wire",
                "WireType, overwrite value specified in yaml config, can be either Satori or SocketCluster",
                CommandOptionType.SingleOrNoValue
            );

            OptionNLogConfig = app.Option(
                "-l|--logconfig",
                "Loads user defined NLog config, -l=user.nlog.config",
                CommandOptionType.SingleValue
            );
        }

        private static void updateLoggingLevel()
        {
            if (OptionGoogleLoggerMinlevel.HasValue() && null != OptionGoogleLoggerMinlevel.Value())
            {
                GoogleLoggerMinlevel = LogLevel.FromString(OptionGoogleLoggerMinlevel.Value());
            }

            if (OptionConsoleLoggerMinlevel.HasValue() && null != OptionConsoleLoggerMinlevel.Value())
            {
                ConsoleLoggerMinlevel = LogLevel.FromString(OptionConsoleLoggerMinlevel.Value());
            }

            if (OptionFileLoggerMinlevel.HasValue() && null != OptionFileLoggerMinlevel.Value())
            {
                FileLoggerMinlevel = LogLevel.FromString(OptionFileLoggerMinlevel.Value());
            }
        }

        public static void ShutdownLogger()
        {
            LogManager.Flush(TimeSpan.FromSeconds(15));
            LogManager.Shutdown();
        }

        private static void setLoggingLevel(string target, LogLevel logLevel)
        {
            var logTarget = LogManager
                .Configuration
                .AllTargets
                .FirstOrDefault(x => x.Name == target);

            if (null != logTarget)
            {
                var logRule = LogManager
                    .Configuration
                    .LoggingRules
                    .FirstOrDefault(x => x.Targets.Contains(logTarget));

                if (null != logRule)
                {
                    logRule.SetLoggingLevels(logLevel, LogLevel.Fatal);
                }
            }
        }
    }
}