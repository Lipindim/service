﻿namespace Kernel
{
    using System;
    public abstract class ConsoleProgram : ProgramBase
    {

        protected ConsoleProgram()
        {
            this.MessageStream = Console.Out;
        }
        protected override void Initialize()
        {
            base.Initialize();
            Console.Title = CfgKernel.EnvironmentName;
            this.PrintWelcomeMessage();
            Console.CancelKeyPress += (sender, e) =>
            {
                e.Cancel = true;
                this.MessageStream?.WriteLine("Ctrl+C, terminating...");
                this.Terminate();
            };
        }
        protected override void OnError(Exception ex)
        {
            Console.Error.WriteLine($"{this.Name} finished with error: {ex}.");
        }
    }
}