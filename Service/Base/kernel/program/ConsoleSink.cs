﻿namespace Kernel
{
    using System;
    using System.IO;

    /// <summary>
    /// Disables console output.
    /// </summary>
    public class ConsoleSink : IDisposable
    {
        private readonly TextWriter textWriter;

        public ConsoleSink()
        {
            textWriter = Console.Out;
            Console.SetOut(TextWriter.Null);
        }

        public void Dispose()
        {
            Console.SetOut(textWriter);
        }
    }
}
