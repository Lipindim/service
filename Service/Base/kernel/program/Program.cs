﻿namespace Kernel
{
    using System;
    using System.Linq;
    using System.Reflection;
    using NLog;

    public static class Program
    {
        private static object obj = new object();
        private static string appId = null;
        private static string version = null;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        /// <summary>
        /// Specify EntryAssemblyName explicitly in case Assembly.GetEntryAssembly returns null.
        /// It happens if program started from unmanaged code (say as COM object from Excel)
        /// </summary>
        public static string EntryAssemblyName { get; set; }

        private static Assembly entryAssembly;

        /// <summary>
        /// return EntryAssembly, Assembly.GetEntryAssembly can be null, if program started from unmanaged code (say as COM object from Excel)
        /// in such case you can explicitly specify its name with EntryAssemblyName
        /// </summary>
        public static Assembly EntryAssembly
        {
            get
            {
                if (entryAssembly == null)
                {
                    lock (obj)
                    {
                        if (entryAssembly == null)
                        {
                            entryAssembly = Assembly.GetEntryAssembly();
                            if (entryAssembly == null)
                            {
                                entryAssembly = AppDomain.CurrentDomain.GetAssemblies()
                                    .SingleOrDefault(a => a.GetName().Name == EntryAssemblyName);
                                if (entryAssembly == null)
                                    throw new Exception(
                                        $"Entry assembly is null and no assembly with name {EntryAssemblyName} was found");
                            }
                        }
                    }
                }

                return entryAssembly;
            }
        }

        public static string AppId
        {
            get
            {
                if (appId == null)
                {
                    lock (obj)
                    {
                        if (appId == null)
                        {
                            if (Environment.UserName == "root")
                                appId = EntryAssembly.GetName().Name + "." + RndId; // don't add "root" as username, save space
                            else
                                appId = EntryAssembly.GetName().Name + "." + Environment.UserName + "." + RndId;
                            logger.Info("Generated AppId: " + appId);
                        }
                    }
                }
                return appId;
            }
        }

        public static string Version
        {
            get
            {
                if (string.IsNullOrWhiteSpace(version))
                {
                    version = Program.EntryAssembly.GetName().Version.ToString();
                }
                return version;
            }
        }

        static string rndId = null;
        public static string RndId
        {
            get
            {
                if (rndId == null)
                {
                    lock (obj)
                    {
                        if (rndId == null)
                        {
                            rndId = RandomString(10);// don't contract it to 6 to avoid collisions
                        }
                    }
                }
                return rndId;
            }
        }


        private static readonly Random random = new Random();

        /// <summary>
        /// Generate human readable random string
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="length">Length.</param>
        public static string RandomString(int length)
        {
            if (length < 2)
                throw new Exception("Id length is too small");

            const string vowel = "aeiouy";
            const string consonants = "bcdfghjklmnpqrstvwx";
            const string digits = "0123456789";

            char[] buffer = new char[length];

            int i = 0;

            for (; i < length - 2; ++i)
            {
                if (random.Next(4) % 3 == 0)
                {
                    buffer[i] = consonants[random.Next(consonants.Length)];
                    if (random.Next(4) == 0)
                        buffer[i] = char.ToUpper(buffer[i]);
                }
                else
                {
                    buffer[i] = vowel[random.Next(vowel.Length)];
                    if (random.Next(4) == 0)
                        buffer[i] = char.ToUpper(buffer[i]);
                }
            }

            for (; i < length; ++i)
            {
                buffer[i] = digits[random.Next(digits.Length)];
            }

            return new string(buffer);
#if false
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxy";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
#endif
        }
    }
}